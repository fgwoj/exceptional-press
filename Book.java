/**
 * A data structure to represent an individual Book.
 * @author Franciszek Wojciechowski
 */

public class Book {
    private String title;
    private String author;
    private String content;
    private int edition;

    /**
     * Returns a book's title from a file.
     * @return Book's title.
     */
    public String getTitle(){
        return title;
    }
    /**
     * Returns a book's author from a file.
     * @return Book's author.
     */
    public String getAuthor(){
        return author;
    }
    /**
     * Returns a book's content from a file.
     * @return Book's content.
     */
    public String getContent(){
        return content;
    }
    /**
     * Process an alphabet order according to the default shift.
     * @return Book's Edition.
     */
    public int getEdition(){
        return edition;
    }
    /**
     * Processes the length of a book's content to get an estimated amount of pages.
     * @return Number of book's pages.
     */
    public int getPages(){
        return (int) content.length() / 800;
    }
    /**
     * Converts all information to a string.
     * @return Final string with information about a book.
     */
    public String toString(){
        String page = "" + getPages();
        String text = "Title: %s\nAuthor: %s\nEdition: %s\nPages: %s\n".formatted(title,author,edition,page); 
        return text;
    }
    /**
     * Retrieve an input with information about a book.
     * @param t Title of a book.
     * @param a Author of a book.
     * @param c Content from a book.
     * @param e Edition of a book.
     */
    public Book(String t, String a, String c, int e){
        title = t;
        author = a;
        content = c;
        edition = e;
    }

}