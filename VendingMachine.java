/**
 * Sell books to customers
 * @author Franciszek Wojciechowski
 */

import java.util.*;

public class VendingMachine{
    private Press supplier;
    private double locationFactor;
    private double cassette;
    private int size;


    /**
     * Retrieve amount of money in cassette
     * @return Amount of money in the cassette.
     */
    public double getCassette(){       
        return cassette;
    }

    /**
     * Saves the specific "coin" into the cassette.
     * @param coin Alphabet order according to the default shift.
     * @return Returns regular alphabet string.
     */
    public void insertCoin(double coin){
        if(coin == 0.01){
            cassette += coin;
        }else if(coin == 0.02){
            cassette += coin;
            
        }else if(coin == 0.05){
            cassette += coin;
            
        }else if(coin == 0.01){
            cassette += coin;
            
        }else if(coin == 0.1){
            cassette += coin;
            
        }else if(coin == 0.2){
            cassette += coin;
            
        }else if(coin == 0.5){
            cassette += coin;
            
        }else if(coin == 1.00){
            cassette += coin;
            
        }else if(coin == 2.00){
            cassette += coin;
            
        }else{
            throw new IllegalArgumentException();
        }
    }

    /**
     * Process a alphabet order according to the deafult shift.
     * @return Returns the rest of the money that was left after purchased the book.
     */
    public double returnCoins(){
        double save = cassette;
        cassette = 0;
        return save;
    }

    /**
     * Procces transaction of a book.
     * @param bookID The name of requested book (a file).
     * @return Bought book.
     */
    public Book buyBook(String bookID){
        if(supplier.getShelf().containsKey(bookID)){
            List<Book> s = new LinkedList<Book>(supplier.getShelf().get(bookID));

            if(supplier.getCatalogue().contains(bookID)){
                double price = s.get(0).getPages() * locationFactor;
                if(price <= getCassette()){
                    cassette -= price;
                    supplier.request(bookID, size);
                    
                }else{
                    throw new RuntimeException();
                }
            }
            
            Book e = new Book(s.get(0).getTitle(), s.get(0).getAuthor(), s.get(0).getContent(), s.get(0).getEdition());
            return e;

        }else{
            throw new IllegalArgumentException();
        }

    }  

    /**
     * Process all necessary information that will be used in a vending machine.
     * @param supplier Gets all books from directory.
     * @param locationFactor Determinates price for specific area.
     * @param size Requests a number of books
     */
    public VendingMachine(Press supplier, double locationFactor, int size){
        this.supplier = supplier;
        this.locationFactor = locationFactor;
        this.size = size;
   }

}