/**
 * Used to create books.
 * @author Franciszek Wojciechowski
 */

import java.util.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Press {
    private int booksPerEdition;
    private Map<String,Integer> edition = new HashMap<String, Integer>();
    private Map<String, Queue<Book>> shelf = new HashMap<>();
    private Map<String,String> texts = new HashMap<>();

    /**
     * Lists all books which are stored on the shelf.
     * @return All books in shelf.
     */
    public Map<String, Queue<Book>> getShelf(){
        return shelf;
    }

    /**
     * Lists all bookIDs which are stored on the shelf.
     * @return All bookIDs stored in shelf. 
     */
    public List<String> getCatalogue(){
        List<String> p = new ArrayList<String>(shelf.keySet());

        return p;
    }

    /**
     * Gets the specific amount of books from the shelf.
     * @param bookID The name of requested book (a file).
     * @param amount Number of requested books.
     * @return Return requested books.
     */
    public List<Book> request(String bookID, int amount){
        if(shelf.containsKey(bookID)){
        List<Book> p = new LinkedList<Book>(shelf.get(bookID));
        List<Book> a = new LinkedList<>();
        Queue<Book> d = new LinkedList<Book>(shelf.get(bookID));

            for(int i = 0; i < amount; i++){
                if(d.size() > 0){
                    Book e = new Book(p.get(0).getTitle(),p.get(0).getAuthor(),p.get(0).getContent(),p.get(0).getEdition());                
                    a.add(e);
                    d.remove();
                }else{
                    Book e = new Book(p.get(0).getTitle(),p.get(0).getAuthor(),p.get(0).getContent(),p.get(0).getEdition()+1);
                    List<Book> z = new ArrayList<Book>();
                    z.add(e);
                    Queue<Book> queue = new LinkedList<>(z);
                    shelf.put(bookID, queue);
                    d = new LinkedList<Book>(shelf.get(bookID));
                    a.add(e);
                    d.remove();
                } 
            }
            return a;
        }else{
            throw new IllegalArgumentException();
        }
    }

    /**
     * Prints an information about specific book in shelf.
     * @param bookID The name of requested book (a file).
     * @return List about a specified book.
     */
    private List<Book> print(String bookID) {
        if(shelf.containsKey(bookID)){
            List<Book> list = new ArrayList<Book>(shelf.get(bookID));
            return list;
        }else{
            throw new IllegalArgumentException();
        }
    }

    /**
     * Finds all books in a directory, gets all necessary information about each book and puts them on the shelf. 
     * @param pathToBooKDir The directory where the books are stored.
     * @param booksPerEdition The number of books that should be placed on the shelf.
     */
    public Press(String pathToBooKDir, int booksPerEdition){
        this.booksPerEdition = booksPerEdition;
        String sep = File.separator;
        String nameOfFile;
        try{
            File directoryPath = new File(pathToBooKDir);
            File filesList[] = directoryPath.listFiles();
            
            for(File file : filesList) {
                nameOfFile = file.getName();
                Path fileHandler =  Path.of(pathToBooKDir + sep + nameOfFile);
                
                texts.put(nameOfFile,Files.readString(fileHandler));   
               
                Pattern b = Pattern.compile("Title: (.*)");
                Pattern c = Pattern.compile("Author: (.*)");
                String actual = texts.get(nameOfFile);
                Matcher m = b.matcher(actual);
                Matcher e = c.matcher(actual);
                String title = "";
                String author = "";
                if (m.find()) {
                    title += m.group(1);
                    if(e.find()){
                        author += e.group(1);
                    }
                }else{
                    throw new IOException();
                }
                String remLast = actual.substring(actual.indexOf(" ***")+1);
                remLast.trim();
                String remN = remLast.substring(remLast.indexOf("\n")+1);
                remN.trim();
        
                actual = remN;
                edition.put(nameOfFile, 1);
                Book d = new Book(title,author,actual,1);
                List<Book> p = new ArrayList<Book>();
                for(int i = 0; i < booksPerEdition; i++){
                    p.add(d);
                }
                Queue<Book> queue = new LinkedList<>(p);
                shelf.put(nameOfFile, queue);
            }

        }catch(Exception e){
            System.out.println(e);
        }
    }
    
}